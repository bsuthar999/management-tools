#!/bin/bash

chown -R craft:craft /home/craft/accounting-server/files

function checkEnv() {
  if [[ -z "$POSTGRESDB_HOST" ]]; then
    echo "POSTGRESDB_HOST is not set"
    exit 1
  fi
  if [[ -z "$POSTGRESDB_NAME" ]]; then
    echo "POSTGRESDB_NAME is not set"
    exit 1
  fi
  if [[ -z "$POSTGRESDB_USER" ]]; then
    echo "POSTGRESDB_USER is not set"
    exit 1
  fi
  if [[ -z "$POSTGRESDB_PASSWORD" ]]; then
    echo "POSTGRESDB_PASSWORD is not set"
    exit 1
  fi
  if [[ -z "$MONGODB_HOST" ]]; then
    echo "MONGODB_HOST is not set"
    exit 1
  fi
  if [[ -z "$MONGODB_NAME" ]]; then
    echo "MONGODB_NAME is not set"
    exit 1
  fi
  if [[ -z "$BULL_QUEUE_REDIS_HOST" ]]; then
    echo "BULL_QUEUE_REDIS_HOST is not set"
    exit 1
  fi
    if [[ -z "$BULL_QUEUE_REDIS_PORT" ]]; then
    echo "BULL_QUEUE_REDIS_HOST is not set"
    exit 1
  fi
  if [[ -z "$NODE_ENV" ]]; then
    echo "NODE_ENV is not set"
    exit 1
  fi
}

function checkConnection() {
  # Wait for postgres
  dockerize -wait tcp://$POSTGRESDB_HOST:5432 -timeout 30s

  # Wait for mongodb
  dockerize -wait tcp://$MONGODB_HOST:27017 -timeout 30s

  # Wait for redis for bull queue
  dockerize -wait tcp://$BULL_QUEUE_REDIS_HOST:$BULL_QUEUE_REDIS_PORT -timeout 30s
}

function configureServer() {
  if [ ! -f .env ]; then
    dockerize -template docker/env.tmpl:.env
  fi
  if [ ! -f ormconfig.json ]; then
    dockerize -template docker/ormconfig.tmpl:ormconfig.json
  fi
}
export -f configureServer

if [ "$1" = 'rollback' ]; then
  # Validate if DB_HOST is set.
  checkEnv
  # Validate DB Connection
  checkConnection
  # Configure server
  su craft -c "bash -c configureServer"
  # Rollback Migrations
  echo "Rollback Migrations"
  su craft -c "./node_modules/.bin/typeorm migration:revert"
fi

if [ "$1" = 'start' ]; then
  # Validate if DB_HOST is set.
  checkEnv
  # Validate DB Connection
  checkConnection
  # Configure server
  su craft -c "bash -c configureServer"
  # Run Migrations
  echo "Run Migrations"
  su craft -c "./node_modules/.bin/typeorm migration:run"
  su craft -c "node dist/server/main.js"
fi

exec runuser -u craft "$@"
