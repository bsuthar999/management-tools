import { Module, Global } from '@nestjs/common';
import { ConnectionService } from './connection/connection.service';

@Global()
@Module({
  providers: [ConnectionService],
  exports: [ConnectionService],
})
export class CommonModule {}
