import { Test, TestingModule } from '@nestjs/testing';
import { ConnectionService } from './connection.service';
import { getConnectionToken } from '@nestjs/typeorm';

describe('ConnectionService', () => {
  let service: ConnectionService;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ConnectionService,
        {
          provide: getConnectionToken('mongodb'),
          useValue: {},
        },
        {
          provide: getConnectionToken('postgres'),
          useValue: {},
        },
      ],
    }).compile();
    service = module.get<ConnectionService>(ConnectionService);
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
