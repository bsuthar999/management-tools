import { Column, Entity, BaseEntity, ObjectIdColumn, ObjectID } from 'typeorm';
import * as uuidv4 from 'uuid/v4';

@Entity()
export class Settings extends BaseEntity {
  @ObjectIdColumn()
  _id: ObjectID;

  @Column()
  uuid: string;

  @Column()
  appURL: string;

  @Column()
  authServerURL: string;

  @Column()
  clientId: string;

  @Column()
  clientSecret: string;

  @Column()
  profileURL: string;

  @Column()
  tokenURL: string;

  @Column()
  accountSetup: boolean = false;

  @Column()
  introspectionURL: string;

  @Column()
  authorizationURL: string;

  @Column()
  periodClosing: string = 'monthly';

  @Column()
  callbackURLs: string[];

  @Column()
  revocationURL: string;

  constructor() {
    super();
    if (!this.uuid) this.uuid = uuidv4();
  }
}
