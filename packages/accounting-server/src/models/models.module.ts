import { Module, Global } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Account } from './account/account.entity';
import { TokenCache } from './token-cache/token-cache.collection';
import { TokenCacheService } from './token-cache/token-cache.service';
import { Settings } from './settings/settings.collection';
import { SettingsService } from './settings/settings.service';
import { AccountStatementService } from './account-statement/account-statement.service';
import { GeneralLedgerArchiveService } from './general-ledger-archive/general-ledger-archive.service';
import { JournalEntryAccountService } from './journal-entry-account/journal-entry-account.service';
import { JournalEntryService } from './journal-entry/journal-entry.service';
import { AccountService } from './account/account.service';
import { GeneralLedgerService } from './general-ledger/general-ledger.service';
import { AccountStatement } from './account-statement/account-statement.entity';
import { GeneralLedger } from './general-ledger/general-ledger.entity';
import { GeneralLedgerArchive } from './general-ledger-archive/general-ledger-archive.entity';
import { JournalEntry } from './journal-entry/journal-entry.entity';
import { JournalEntryAccount } from './journal-entry-account/journal-entry-account.entity';

@Global()
@Module({
  imports: [
    TypeOrmModule.forFeature(
      [
        Account,
        AccountStatement,
        GeneralLedger,
        GeneralLedgerArchive,
        JournalEntry,
        JournalEntryAccount,
      ],
      'postgres',
    ),
    TypeOrmModule.forFeature([Settings, TokenCache], 'mongodb'),
  ],
  providers: [
    SettingsService,
    TokenCacheService,
    AccountStatementService,
    GeneralLedgerArchiveService,
    JournalEntryAccountService,
    JournalEntryService,
    AccountService,
    GeneralLedgerService,
  ],
  exports: [
    SettingsService,
    TokenCacheService,
    AccountStatementService,
    GeneralLedgerArchiveService,
    JournalEntryAccountService,
    JournalEntryService,
    AccountService,
    GeneralLedgerService,
  ],
})
export class ModelsModule {}
