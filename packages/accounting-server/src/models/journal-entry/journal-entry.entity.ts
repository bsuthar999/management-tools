import {
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  OneToMany,
  BaseEntity,
  Column,
} from 'typeorm';
import { JournalEntryAccount } from '../journal-entry-account/journal-entry-account.entity';
import { GeneralLedger } from '../general-ledger/general-ledger.entity';

@Entity()
export class JournalEntry extends BaseEntity {
  @PrimaryGeneratedColumn()
  transactionId: number;

  @CreateDateColumn()
  transactionDate: Date;

  @Column({ type: 'boolean' })
  isLocked: boolean = true;

  @OneToMany(type => JournalEntryAccount, account => account.journalEntry, {
    cascade: true,
  })
  accounts: JournalEntryAccount[];

  @OneToMany(
    type => GeneralLedger,
    generalLedger => generalLedger.journalEntry,
    {
      cascade: true,
    },
  )
  generalLedgerJournalEntries: GeneralLedger[];
}
