import { Test, TestingModule } from '@nestjs/testing';
import { JournalEntryService } from './journal-entry.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { JournalEntry } from './journal-entry.entity';

describe('JournalEntryService', () => {
  let service: JournalEntryService;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        JournalEntryService,
        {
          provide: getRepositoryToken(JournalEntry),
          useValue: {},
        },
      ],
    }).compile();
    service = module.get<JournalEntryService>(JournalEntryService);
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
