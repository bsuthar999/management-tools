import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { JournalEntry } from './journal-entry.entity';
import { Repository } from 'typeorm';

@Injectable()
export class JournalEntryService {
  constructor(
    @InjectRepository(JournalEntry)
    private readonly jeRepository: Repository<JournalEntry>,
  ) {}

  async getAll(limit, offset, search, sort, query) {
    let journalEntry;
    if (!search) {
      journalEntry = await this.jeRepository
        .createQueryBuilder('journal_entry')
        .orderBy(`"transactionId"`, sort)
        .skip(offset)
        .take(limit)
        .getMany();
      const out = {
        docs: journalEntry,
        offset,
        limit,
      };
      return out;
    } else {
      journalEntry = await this.jeRepository
        .createQueryBuilder('journal_entry')
        .orderBy(`"transactionId"`, sort)
        .where(`( "transactionId" = '${search}' )`)
        .skip(offset)
        .take(limit)
        .getMany();
      const out = {
        docs: journalEntry,
        offset,
        limit,
      };
      return out;
    }
  }

  async create(payload) {
    return await this.jeRepository.save(payload);
  }
}
