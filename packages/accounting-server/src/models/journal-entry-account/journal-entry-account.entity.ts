import {
  Entity,
  ManyToOne,
  JoinColumn,
  Column,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { JournalEntry } from '../journal-entry/journal-entry.entity';
import { Account } from '../account/account.entity';
import { TransactionType } from '../general-ledger/general-ledger.entity';

@Entity()
export class JournalEntryAccount {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(type => JournalEntry, entry => entry.accounts)
  journalEntry: JournalEntry;

  @ManyToOne(type => Account, account => account.journalEntryAccounts)
  @JoinColumn()
  account: Account;

  @Column({ type: 'enum', enum: ['CREDIT', 'DEBIT'] })
  entryType: TransactionType;

  @Column({ type: 'float8' })
  amount: number;

  @Column()
  createdBy: string;
}
