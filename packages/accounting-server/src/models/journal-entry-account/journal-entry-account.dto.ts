import { Account } from '../account/account.entity';
import { TransactionType } from '../general-ledger/general-ledger.entity';
import { IsNotEmpty, IsNumber } from 'class-validator';

export class JournalEntryAccountDto {
  @IsNotEmpty()
  account: Account;

  @IsNotEmpty()
  entryType: TransactionType;

  @IsNumber()
  amount: number;
}
