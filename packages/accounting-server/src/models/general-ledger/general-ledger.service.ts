import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { GeneralLedger } from './general-ledger.entity';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class GeneralLedgerService {
  constructor(
    @InjectRepository(GeneralLedger)
    private readonly repository: Repository<GeneralLedger>,
  ) {}

  getRepository() {
    return this.repository;
  }
}
