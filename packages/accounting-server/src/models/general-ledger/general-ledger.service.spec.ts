import { Test, TestingModule } from '@nestjs/testing';
import { GeneralLedgerService } from './general-ledger.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { GeneralLedger } from './general-ledger.entity';

describe('GeneralLedgerService', () => {
  let service: GeneralLedgerService;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        GeneralLedgerService,
        {
          provide: getRepositoryToken(GeneralLedger),
          useValue: {},
        },
      ],
    }).compile();
    service = module.get<GeneralLedgerService>(GeneralLedgerService);
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
