import { Injectable, BadRequestException } from '@nestjs/common';
import { Account } from './account.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { TreeRepository } from 'typeorm';
import { ACCOUNT_TYPES } from './../../constants/app-strings';
import * as uuidv4 from 'uuid/v4';

@Injectable()
export class AccountService {
  accountChildrens: string[] = [];
  constructor(
    @InjectRepository(Account)
    private readonly accountRepository: TreeRepository<Account>,
  ) {}

  async find(limit, offset, search, sort, query) {
    let account;
    if (!search) {
      account = await this.accountRepository
        .createQueryBuilder('account') // first argument is an alias. Alias is what you are selecting - photos. You must specify it.
        .orderBy(`"accountName"`, sort)
        .where(query)
        .skip(offset)
        .take(limit)
        .getMany();
      const out = {
        docs: account,
        offset,
        length: await this.accountRepository.count(),
      };

      return out;
    } else {
      search = search;
      account = await this.accountRepository
        .createQueryBuilder('account') // first argument is an alias. Alias is what you are selecting - photos. You must specify it.
        .orderBy(`"accountName"`, sort)
        .where('"accountName" LIKE :search', { search: `%${search}%` })
        .orWhere('"accountNumber" LIKE :search', { search: `%${search}%` })
        .orWhere('"accountType" LIKE :search', { search: `%${search}%` })
        .skip(offset)
        .take(limit)
        .getMany();
      const out = {
        docs: account,
        offset,
        length: await this.accountRepository.count(),
      };

      return out;
    }
    // } else {
    //   account = await this.accountRepository
    //     .createQueryBuilder('account') // first argument is an alias. Alias is what you are selecting - photos. You must specify it.
    //     .orderBy(`"accountName"`, sort)
    //     .where(`( "accountName" = '${search}' )`)
    //     .skip(offset)
    //     .take(limit)
    //     .setParameters({ search })
    //     .getMany();
    //   return account;
    // }
  }

  async create(payload, req) {
    const createdBy = req.token.sub;
    const account = new Account();
    const isRoot = false;
    const parent = await this.findParent(payload.parent);
    payload.parent = parent[0].id;
    if (this.validateAccount(payload.accountType, payload.parent)) {
      account.uuid = uuidv4();
      await this.accountRepository.query(
        `INSERT INTO "account"
        ("uuid", "accountNumber", "accountName", "accountType","isRoot", "isGroup", "createdBy", "modifiedBy", "modified", "parentId")
        VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)`,
        [
          account.uuid,
          payload.accountNumber,
          payload.accountName,
          payload.accountType,
          isRoot,
          payload.isGroup,
          createdBy,
          createdBy,
          'NOW()',
          payload.parent,
        ],
      );
    } else {
      throw new BadRequestException();
    }
    await this.createAccountClosure(payload.parent, account.uuid);
    return { uuid: account.uuid };
  }

  async findOne(params): Promise<any> {
    if (params.uuid) {
      return await this.findAccountByUUID(params.uuid);
    } else {
      return await this.accountRepository.findOne(params);
    }
  }

  async findAccountByUUID(uuid) {
    const account = await this.accountRepository.findOne({ uuid });
    if (account.isRoot === true) {
      return account;
    }
    const parentId = await this.accountRepository.query(
      'SELECT "parentId" FROM account WHERE uuid = $1',
      [uuid],
    );

    const data = parentId.length > 0 ? parentId[0].parentId : null;
    if (!data) throw new BadRequestException();

    const parent = await this.accountRepository.query(
      'SELECT "accountNumber" FROM account WHERE id = $1',
      [data],
    );

    account.parent = parent.length > 0 ? parent[0].accountNumber : null;
    if (!account.parent) throw new BadRequestException();
    return account;
  }

  async findParent(parent) {
    return await this.accountRepository.query(
      'select id from account where "accountNumber" = $1',
      [parent],
    );
  }

  async getRoots() {
    return this.accountRepository.query(
      `SELECT "accountNumber" FROM account WHERE id < 6`,
    );
  }

  async update(payload, req) {
    const modifiedBy = req.token.sub;
    await this.accountRepository.query(
      `UPDATE account
      SET "accountName" = $1 , "modifiedBY"= $3, "modified" = $4
      WHERE "uuid" = $2`,
      [payload.accountName, payload.uuid, modifiedBy, 'NOW()'],
    );
    return this.accountRepository.query(
      `SELECT * FROM account WHERE uuid ='${payload.uuid}'`,
    );
  }

  validateAccount(payloadAccountType, payloadAccountParent) {
    if (ACCOUNT_TYPES.includes(payloadAccountType) && payloadAccountParent) {
      return true;
    }
    return false;
  }

  async createAccountClosure(parent, uuid) {
    const child = await this.accountRepository.query(
      `select id from account where uuid = $1 `,
      [uuid],
    );
    await this.accountRepository.query(
      `INSERT into "account_closure" ("id_ancestor" , "id_descendant") VALUES ($1, $2)`,
      [child[0].id, parent],
    );
  }

  async getTree(accountNumber) {
    this.accountChildrens = [];
    const parentId = await this.accountRepository.query(
      `SELECT id FROM account where "accountNumber" = $1`,
      [accountNumber],
    );
    const childrens = await this.accountRepository.query(
      `SELECT id_ancestor FROM account_closure WHERE (id_descendant = $1) and (id_ancestor != $1);`,
      [parentId[0].id],
    );
    for (const child of childrens) {
      const accountNo = await this.accountRepository.query(
        `SELECT "accountNumber" FROM account WHERE id = $1 `,
        [child.id_ancestor],
      );
      this.accountChildrens.push(accountNo[0]);
    }
    return this.accountChildrens;
  }

  async getLastClosing(id) {
    return await this.getRepository().query(
      `
      SELECT "closingBalance"
      FROM account_statement WHERE "lastUpdated" IN
      (SELECT MAX("lastUpdated") FROM account_statement WHERE "accountId" = $1 )
      `,
      [id],
    );
  }

  async getAccountId() {
    return await this.getRepository().query(`
    SELECT id FROM account where "isGroup" = 'false'
    `);
  }

  getRepository() {
    return this.accountRepository;
  }
}
