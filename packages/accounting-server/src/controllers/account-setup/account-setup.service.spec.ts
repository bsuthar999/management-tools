import { Test, TestingModule } from '@nestjs/testing';
import { AccountSetupService } from './account-setup.service';
import { TokenCacheService } from '../../models/token-cache/token-cache.service';
import { SettingsService } from '../../models/settings/settings.service';
import { AccountService } from '../../models/account/account.service';

describe('AccountSetupService', () => {
  let service: AccountSetupService;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AccountSetupService,
        {
          provide: SettingsService,
          useValue: {},
        },
        {
          provide: AccountService,
          useValue: {},
        },
        {
          provide: TokenCacheService,
          useValue: {},
        },
      ],
    }).compile();
    service = module.get<AccountSetupService>(AccountSetupService);
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
