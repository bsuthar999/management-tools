import { Test, TestingModule } from '@nestjs/testing';
import { AccountSetupController } from './account-setup.controller';
import { AccountSetupService } from './account-setup.service';
import { AuthServerVerificationGuard } from '../../guards/authserver-verification.guard';
import { TokenGuard } from '../../guards/token.guard';

describe('AccountSetup Controller', () => {
  let module: TestingModule;

  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [AccountSetupController],
      providers: [
        {
          provide: AccountSetupService,
          useValue: {},
        },
      ],
    })
      .overrideGuard(AuthServerVerificationGuard)
      .useValue({})
      .overrideGuard(TokenGuard)
      .useValue({})
      .compile();
  });
  it('should be defined', () => {
    const controller: AccountSetupController = module.get<
      AccountSetupController
    >(AccountSetupController);
    expect(controller).toBeDefined();
  });
});
