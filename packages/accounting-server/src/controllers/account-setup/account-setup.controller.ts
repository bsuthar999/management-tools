import {
  Controller,
  UseGuards,
  Post,
  Req,
  BadRequestException,
} from '@nestjs/common';
import { AccountSetupService } from './account-setup.service';
import { TokenGuard } from '../../guards/token.guard';

@Controller('account-setup')
export class AccountSetupController {
  constructor(private readonly accountSetupService: AccountSetupService) {}

  @Post('v1/oneTimeSetup')
  @UseGuards(TokenGuard)
  async accountSetup(@Req() req) {
    if (await this.accountSetupService.oneTimeSetup(req)) {
      return true;
    }
    return BadRequestException;
  }
}
