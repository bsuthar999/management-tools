import { Injectable } from '@nestjs/common';
import * as uuidv4 from 'uuid/v4';
import { SettingsService } from '../../models/settings/settings.service';
import { AccountService } from '../../models/account/account.service';

@Injectable()
export class AccountSetupService {
  constructor(
    private readonly settingService: SettingsService,
    private readonly accountService: AccountService,
  ) {}
  async oneTimeSetup(req) {
    const settings = await this.settingService.find();
    const creator = req.token.sub;
    const ata = 1;
    await this.accountService.getRepository().query(
      `INSERT INTO "account"
            ("uuid", "accountNumber", "accountName", "accountType", "isRoot", "isGroup" , "createdBy" , "modifiedBy","modified") VALUES
              ('${uuidv4()}', '100000000', 'Asset', 'ASSET', true, true, '${creator}', '${creator}', current_timestamp ),
              ('${uuidv4()}', '200000000', 'Liability', 'LIABILITY', true, true, '${creator}', '${creator}', current_timestamp ),
              ('${uuidv4()}', '300000000', 'Equity', 'EQUITY', true, true, '${creator}', '${creator}', current_timestamp ),
              ('${uuidv4()}', '400000000', 'Income', 'INCOME', true, true, '${creator}', '${creator}', current_timestamp ),
              ('${uuidv4()}', '500000000', 'Expense', 'EXPENSE', true, true, '${creator}', '${creator}', current_timestamp )`,
    );
    await this.accountService
      .getRepository()
      .query(
        `insert into account_closure (id_ancestor , id_descendant) VALUES (1 , 1), (2 , 2), (3 , 3), (4 , 4), (5 , 5)`,
      );
    if (ata) {
      settings.accountSetup = true;
      return true;
    }
    return false;
  }
}
