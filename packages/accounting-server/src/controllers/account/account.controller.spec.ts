import { Test, TestingModule } from '@nestjs/testing';
import { AccountController } from './account.controller';
import { AccountService } from '../../models/account/account.service';
import { AuthServerVerificationGuard } from '../../guards/authserver-verification.guard';
import { TokenGuard } from '../../guards/token.guard';

describe('Account Controller', () => {
  let module: TestingModule;

  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [AccountController],
      providers: [
        {
          provide: AccountService,
          useValue: {
            find: (...args) => {},
            findOne: (...args) => {},
            create: (...args) => {},
          },
        },
      ],
    })
      .overrideGuard(AuthServerVerificationGuard)
      .useValue({})
      .overrideGuard(TokenGuard)
      .useValue({})
      .compile();
  });
  it('should be defined', () => {
    const controller: AccountController = module.get<AccountController>(
      AccountController,
    );
    expect(controller).toBeDefined();
  });
});
