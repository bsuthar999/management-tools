import { IsOptional, IsString, IsBoolean, IsNumber } from 'class-validator';

export class AccountDto {
  @IsOptional()
  @IsString()
  uuid: string;

  @IsString()
  accountNumber: string;

  @IsString()
  accountName: string;

  @IsString()
  accoountType: string;

  @IsBoolean()
  isGroup: boolean;

  @IsString()
  createdBy: string;

  @IsNumber()
  parent: string;
}
