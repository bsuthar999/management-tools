import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Put,
  Req,
  Query,
  UseGuards,
} from '@nestjs/common';
import { AccountService } from '../../models/account/account.service';
import { AccountDto } from './account-dto';
import { TokenGuard } from '../../guards/token.guard';

@Controller('account')
export class AccountController {
  constructor(private readonly accountService: AccountService) {}

  @Post('v1/create')
  @UseGuards(TokenGuard)
  async createAccount(@Body() payload, @Req() req) {
    return await this.accountService.create(payload, req);
  }

  @Get('v1/list')
  @UseGuards(TokenGuard)
  async list(
    @Req() req,
    @Query('limit') limit: number = 10,
    @Query('offset') offset: number = 0,
    @Query('search') search?: string,
    @Query('sort') sort?: string,
    @Query('filters') filters?,
  ) {
    // filters = [
    //   ["date", ">", "2000"],
    //   ["date", ">", "1999"],
    //   ["isGroup", "=", true],
    // ]
    // filters.forEach(condition => {
    //    condition.forEach( element => {
    //      if(element in OPERATORS){
    //        let query  = " '" + element + "' ";
    //        console.log(query);

    //       }
    //       else{
    //         let query = element;
    //         console.log(query);
    //       }

    //    });
    // });

    // if (!(await this.userService.checkAdministrator(req.user.user))) {
    //   query.createdBy = req.user.utser;
    // }

    sort = sort ? sort.toUpperCase() : 'ASC';

    return this.accountService.find(limit, offset, search, sort, filters);
  }

  @Put('v1/update')
  @UseGuards(TokenGuard)
  async updateAccount(@Body() payload, @Req() req) {
    return await this.accountService.update(payload, req);
  }

  @Get('v1/get/:uuid')
  @UseGuards(TokenGuard)
  async getAccount(@Param('uuid') uuid: AccountDto) {
    return await this.accountService.findOne({ uuid });
  }

  @Get('v1/getRoots')
  @UseGuards(TokenGuard)
  async getRoots() {
    return await this.accountService.getRoots();
  }

  @Get('v1/tree/:accountNumber')
  @UseGuards(TokenGuard)
  async getTree(@Param('accountNumber') accountNumber: AccountDto) {
    return await this.accountService.getTree(accountNumber);
  }
}
