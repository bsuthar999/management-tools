import { Test, TestingModule } from '@nestjs/testing';
import { JournalEntryManagementService } from './journal-entry-management.service';
import { JournalEntryService } from '../../models/journal-entry/journal-entry.service';
import { AccountService } from '../../models/account/account.service';
import { JournalEntryAccountService } from '../../models/journal-entry-account/journal-entry-account.service';
import { GeneralLedgerService } from '../../models/general-ledger/general-ledger.service';

describe('JournalEntryManagementService', () => {
  let service: JournalEntryManagementService;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        JournalEntryManagementService,
        {
          provide: JournalEntryService,
          useValue: {},
        },
        {
          provide: GeneralLedgerService,
          useValue: {},
        },
        {
          provide: AccountService,
          useValue: {},
        },
        {
          provide: JournalEntryAccountService,
          useValue: {},
        },
      ],
    }).compile();
    service = module.get<JournalEntryManagementService>(
      JournalEntryManagementService,
    );
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
