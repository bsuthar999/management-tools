import { Injectable, BadRequestException } from '@nestjs/common';
import { AccountService } from '../../models/account/account.service';
import { getConnection } from 'typeorm';
import { JournalEntryService } from '../../models/journal-entry/journal-entry.service';
import { JournalEntryAccountService } from '../../models/journal-entry-account/journal-entry-account.service';
import { JournalEntry } from '../../models/journal-entry/journal-entry.entity';
import { GeneralLedgerService } from '../../models/general-ledger/general-ledger.service';

@Injectable()
export class JournalEntryManagementService {
  constructor(
    private journalEntryService: JournalEntryService,
    private journalEntryAccountService: JournalEntryAccountService,
    private accountService: AccountService,
    private generalLedgerService: GeneralLedgerService,
  ) {}

  async getAll(limit, offset, search, sort, query) {
    return await this.journalEntryService.getAll(
      limit,
      offset,
      search,
      sort,
      query,
    );
  }

  async getOne(id, limit, offset, search, sort, query) {
    const accounts = await this.journalEntryAccountService
      .getRepository()
      .query(
        `SELECT "entryType" , "amount" , "accountId" from journal_entry_account where "journalEntryTransactionId" = $1`,
        [id],
      );

    for (const account of accounts) {
      const accData = await this.accountService
        .getRepository()
        .query('SELECT "accountNumber" FROM "account" where id = $1', [
          account.accountId,
        ]);
      account.account = accData[0].accountNumber;
    }

    return accounts;
  }

  async create(payload, req) {
    const createdBy = req.token.sub;
    const journalEntryParams = [];
    let account;
    let i = 1;
    let journalEntryQuery =
      'insert into journal_entry_account ( "entryType","amount", "createdBy", "journalEntryTransactionId","accountId") VALUES';

    const journalEntry = await this.validateJE(payload);

    for (account of payload.accounts) {
      const index = await this.accountService.getRepository().query(
        `
      SELECT id FROM account WhERE "accountNumber" = $1
      `,
        [account.account],
      );
      journalEntryParams.push(
        account.entryType,
        account.amount,
        createdBy,
        journalEntry.transactionId,
        index[0].id,
      );
      const sQuery =
        ' ( $' +
        i +
        ', $' +
        (i + 1) +
        ', $' +
        (i + 2) +
        ', $' +
        (i + 3) +
        ', $' +
        (i + 4) +
        '),';
      i = i + 5;
      journalEntryQuery += sQuery;
    }
    journalEntryQuery = journalEntryQuery.slice(0, -1) + ';';

    await this.journalEntryAccountService
      .getRepository()
      .query(journalEntryQuery, journalEntryParams);
    await this.generalLedgerService
      .getRepository()
      .query(journalEntryQuery, journalEntryParams);
    return journalEntry.transactionId;
  }

  async validateJE(payload) {
    const param = [];
    let account,
      creditAmount = 0,
      debitAmount = 0;
    for (account of payload.accounts) {
      param.push(account.account);
      param.join(',');

      if (account.entryType === 'CREDIT') {
        debitAmount += account.amount;
      } else {
        creditAmount += account.amount;
      }
    }
    if (creditAmount === 0 || debitAmount === 0) {
      throw new BadRequestException();
    }
    if (debitAmount !== creditAmount) {
      throw new BadRequestException();
    }

    for (const iterator of param) {
      const response = await this.accountService
        .getRepository()
        .query(
          `SELECT uuid FROM account WHERE "accountNumber" = $1 and "isGroup" = false `,
          [iterator],
        );
      if (response.length === 0) {
        throw new BadRequestException();
      }
    }

    const journalEntry = new JournalEntry();
    const id = await getConnection('postgres')
      .getRepository(JournalEntry)
      .save(journalEntry);
    return id;
  }
}
