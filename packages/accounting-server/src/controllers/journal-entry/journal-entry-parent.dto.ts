import { Type } from 'class-transformer';
import { ValidateNested } from 'class-validator';
import { JournalEntryAccountDto } from '../../models/journal-entry-account/journal-entry-account.dto';

export class JournalEntryDto {
  @ValidateNested()
  @Type(() => JournalEntryAccountDto)
  accounts: JournalEntryAccountDto[];
}
