import {
  Controller,
  Post,
  Param,
  Body,
  Get,
  Query,
  UsePipes,
  ValidationPipe,
  UseGuards,
  Req,
} from '@nestjs/common';
import { JournalEntryManagementService } from './journal-entry-management.service';
import { TokenGuard } from '../../guards/token.guard';
import { JournalEntryDto } from './journal-entry-parent.dto';

@Controller('journalentry')
export class JournalEntryController {
  constructor(
    private readonly managementService: JournalEntryManagementService,
  ) {}

  @Post('v1/create')
  @UseGuards(TokenGuard)
  @UsePipes(ValidationPipe)
  createEntry(@Body() payload: JournalEntryDto, @Req() req) {
    return this.managementService.create(payload, req);
  }

  @Get('v1/getOne/:id')
  @UseGuards(TokenGuard)
  async getOne(
    @Param('id') id: number,
    @Req() req,
    @Query('limit') limit: number = 10,
    @Query('offset') offset: number = 0,
    @Query('search') search?: string,
    @Query('sort') sort?: string,
  ) {
    const query: { createdBy?: string } = {};

    // if (!(await this.userService.checkAdministrator(req.user.user))) {
    //   query.createdBy = req.user.utser;
    // }
    sort = sort ? sort.toUpperCase() : 'ASC';

    return await this.managementService.getOne(
      id,
      limit,
      offset,
      search,
      sort,
      query,
    );
  }

  @Get('v1/list/all')
  @UseGuards(TokenGuard)
  async listEntries(
    @Query('limit') limit: number = 10,
    @Query('offset') offset: number = 0,
    @Query('search') search?: string,
    @Query('sort') sort?: string,
  ) {
    const query: { createdBy?: string } = {};

    // if (!(await this.userService.checkAdministrator(req.user.user))) {
    //   query.createdBy = req.user.utser;
    // }

    sort = sort ? sort.toUpperCase() : 'ASC';
    return await this.managementService.getAll(
      limit,
      offset,
      search,
      sort,
      query,
    );
  }
}
