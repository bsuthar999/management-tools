import { Test, TestingModule } from '@nestjs/testing';
import { AccountReportsController } from './account-reports.controller';
import { AccountReportsService } from './account-reports.service';

describe('AccountReports Controller', () => {
  let module: TestingModule;

  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [AccountReportsController],
      providers: [
        {
          provide: AccountReportsService,
          useValue: {
            getProfitReport: (...args) => {},
          },
        },
      ],
    }).compile();
  });
  it('should be defined', () => {
    const controller: AccountReportsController = module.get<
      AccountReportsController
    >(AccountReportsController);
    expect(controller).toBeDefined();
  });
});
