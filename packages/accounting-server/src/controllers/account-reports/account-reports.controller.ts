import { Controller, Get } from '@nestjs/common';
// import { TokenGuard } from 'guards/token.guard';
import { AccountReportsService } from './account-reports.service';

@Controller('account-reports')
export class AccountReportsController {
  constructor(private readonly accountReportService: AccountReportsService) {}
  @Get('v1/getProfitReport')
  // @UseGuards(TokenGuard)
  async report() {
    return await this.accountReportService.getProfitReport();
  }
}
