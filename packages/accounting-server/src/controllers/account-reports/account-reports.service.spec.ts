import { Test, TestingModule } from '@nestjs/testing';
import { AccountReportsService } from './account-reports.service';
import { AccountService } from '../../models/account/account.service';

describe('AccountReportsService', () => {
  let service: AccountReportsService;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AccountReportsService,
        {
          provide: AccountService,
          useValue: {},
        },
      ],
    }).compile();
    service = module.get<AccountReportsService>(AccountReportsService);
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
