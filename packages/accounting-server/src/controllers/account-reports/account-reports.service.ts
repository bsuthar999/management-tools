import { Injectable } from '@nestjs/common';
import { AccountService } from '../../models/account/account.service';

@Injectable()
export class AccountReportsService {
  constructor(private readonly accountService: AccountService) {}
  async getProfitReport() {
    const liability = [];
    const equity = [];
    const asset = [];

    // for asset accounts
    const assetAccount = await this.accountService.getRepository().query(`
    SELECT id , "accountType" FROM account where "isGroup" = 'false' and "accountType" = 'ASSET'
    `);
    assetAccount.forEach(async element => {
      const single = await this.accountService.getLastClosing(element.id);
      const base = { id: '', closingBalance: 0, accountType: '' };
      base.closingBalance = single[0].closingBalance;
      base.id = element.id;
      base.accountType = element.accountType;
      asset.push(base);
    });

    // for liability accounts
    const liabilityAccount = await this.accountService.getRepository().query(`
    SELECT id , "accountType" FROM account where "isGroup" = 'false' and "accountType" = 'LIABILITY'
    `);
    liabilityAccount.forEach(async element => {
      const base = { id: '', closingBalance: 0, accountType: '' };
      const single = await this.accountService.getLastClosing(element.id);
      base.closingBalance = single[0].closingBalance;
      base.id = element.id;
      base.accountType = element.accountType;
      liability.push(base);
    });

    // for equity accounts
    const equityAccount = await this.accountService.getRepository().query(`
    SELECT id , "accountType" FROM account where "isGroup" = 'false' and "accountType" = 'EQUITY'
    `);
    equityAccount.forEach(async element => {
      const base = { id: '', closingBalance: 0, accountType: '' };
      const single = await this.accountService.getLastClosing(element.id);
      base.closingBalance = single[0].closingBalance;
      base.id = element.id;
      base.accountType = element.accountType;
      equity.push(base);
    });

    return {
      sumOfLiability: liability,
      sumOfAssets: asset,
      sumOfEquity: equity,
    };
  }
}
