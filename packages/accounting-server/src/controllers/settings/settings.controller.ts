import {
  Controller,
  Get,
  UseGuards,
  Post,
  Body,
  Put,
  Query,
} from '@nestjs/common';
import { TokenGuard } from './../../guards/token.guard';
import { SettingsService } from './../../models/settings/settings.service';
import { PeriodClosingDTO } from './period-closing-dto';

@Controller('settings')
export class SettingsController {
  constructor(private readonly settingsService: SettingsService) {}

  @Get('v1/getSettings')
  @UseGuards(TokenGuard)
  async getSettings() {
    return await this.settingsService.findAll();
  }

  @Post('v1/periodClosing')
  @UseGuards(TokenGuard)
  async setPeriodClosing(@Body('body') periodClosing: PeriodClosingDTO) {
    return await this.settingsService.setPeriodClosing(
      periodClosing.periodClosing,
    );
  }

  @Put('v1/update')
  @UseGuards(TokenGuard)
  async updateSettings(@Body('settings ') settings, @Query('query') query) {
    return await this.settingsService.update(query, settings);
  }

  @Post('v1/create')
  @UseGuards(TokenGuard)
  async createSettings(@Body('settings') settings) {
    return await this.settingsService.save(settings);
  }
}
