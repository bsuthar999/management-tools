import { IsString } from 'class-validator';

export class PeriodClosingDTO {
  @IsString()
  periodClosing;
}
