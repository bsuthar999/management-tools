import { Injectable } from '@nestjs/common';
import { OnModuleInit } from '@nestjs/common';
import * as uuidv4 from 'uuid/v4';
import * as Bull from 'bull';
import { BullOptions } from './bull-queue.options';
import { ConfigService } from '../config/config.service';
import { AccountService } from '../models/account/account.service';
import { SettingsService } from '../models/settings/settings.service';
import {
  MONTHLY,
  YEARLY,
  HALF_YEARLY,
  QUATERLY,
} from '../constants/app-strings';
import { getConnection } from 'typeorm';
import { POSTGRES_CONNECTION_NAME } from '../models/postgres.connection';

export const PERIOD_CLOSING = 'period_closing';

@Injectable()
export class PeriodClosingService implements OnModuleInit {
  protected queue: Bull.Queue;
  constructor(
    private readonly configService: ConfigService,
    private readonly settingService: SettingsService,
    private readonly accountService: AccountService,
  ) {
    const bullOptions: BullOptions = {
      redis: {
        host: this.configService.get('BULL_QUEUE_REDIS_HOST'),
        port: Number(this.configService.get('BULL_QUEUE_REDIS_PORT')),
      },
    };
    configService.get('BULL_QUEUE_REDIS_PORT');
    this.queue = new Bull(PERIOD_CLOSING, bullOptions);
  }

  async onModuleInit() {
    this.defineQueueProcess();
    await this.addQueue();
  }

  defineQueueProcess() {
    // validate that its time for peiod closing from mongo settings periodclosing string
    this.queue.process(PERIOD_CLOSING, async (job, done) => {
      const connection = getConnection(POSTGRES_CONNECTION_NAME);
      const queryRunner = connection.createQueryRunner();
      const lastClosing = await this.accountService
        .getRepository()
        .query(`SELECT MAX("lastUpdated") FROM account_statement`);
      const period = await this.settingService.find();

      let nextClosing;
      switch (period.periodClosing) {
        case MONTHLY:
          nextClosing = lastClosing[0].max;
          nextClosing.setDate(nextClosing.getDate() + 30);
          break;

        case QUATERLY:
          nextClosing = lastClosing[0].max;
          nextClosing.setDate(nextClosing.getMonth() + 4);
          break;

        case HALF_YEARLY:
          nextClosing = lastClosing[0].max;
          nextClosing.setDate(nextClosing.getMonth() + 6);
          break;

        case YEARLY:
          nextClosing = lastClosing[0].max;
          nextClosing.setDate(nextClosing.getFullYear() + 1);
          break;
      }
      const date = new Date();
      if (nextClosing > date) {
        return;
      }

      try {
        const perviousClosing = await this.accountService
          .getRepository()
          .query(`SELECT MAX("lastUpdated") FROM account_statement`);

        const accounts = await this.accountService.getAccountId();

        accounts.forEach(async element => {
          let lastUpdated;
          if (perviousClosing[0].max === 'null') {
            lastUpdated = await this.accountService.getRepository().query(
              `
          SELECT creation FROM account WHERE id = $1
          `,
              [element.id],
            );
            lastUpdated = lastUpdated[0].creation;
          } else {
            lastUpdated = perviousClosing[0].max;
          }

          // get credit amount of single account at a time.
          const credit = await this.accountService.getRepository().query(
            `
            SELECT SUM(amount) FROM general_ledger WHERE ("transactionDate" > $1 ) and("accountId" = $2) and ("amountType" = 'CREDIT')
            `,
            [lastUpdated, element.id],
          );

          // get debit amount of single account at a time.
          const debit = await this.accountService.getRepository().query(
            `
            SELECT SUM(amount) FROM general_ledger WHERE ("transactionDate" > $1 ) and("accountId" = $2) and ("amountType" = 'DEBIT')
            `,
            [lastUpdated, element.id],
          );
          const creditAmount = credit[0].sum || 0;
          const debitAmount = debit[0].sum || 0;
          const currentClosingAmount = debitAmount - creditAmount;

          const lastClosingAmount = await this.accountService.getLastClosing(
            element.id,
          );

          // get closing amount of single account from its credit and debit.make an entry for this closing balance
          const closingBalance =
            currentClosingAmount + lastClosingAmount[0].closingBalance;
          const uuid = uuidv4();
          this.accountService.getRepository().query(
            `
        INSERT INTO account_statement ("uuid", "closingBalance", "lastUpdated", "accountId")
        VALUES ($1, $2, $3, $4)
        `,
            [uuid, closingBalance, 'NOW()', element.id],
          );

          // Nice one job done keep doing this for all accounts
        }, done(null, job.id));
      } catch (error) {
        await queryRunner.rollbackTransaction();
        done(error);
      }
      done(null, job.id);
    });
  }

  async addQueue() {
    await this.queue.add(
      PERIOD_CLOSING,
      { message: PERIOD_CLOSING },
      { repeat: { cron: '01 00 * * * *' } },
    );
  }
}
