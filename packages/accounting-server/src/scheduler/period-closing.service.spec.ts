import { Test, TestingModule } from '@nestjs/testing';
import { PeriodClosingService } from './period-closing.service';
import { ConfigService } from '../config/config.service';
import { AccountService } from '../models/account/account.service';
import { SettingsService } from '../models/settings/settings.service';

describe('PeriodClosingService', () => {
  let service: PeriodClosingService;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        PeriodClosingService,
        {
          provide: ConfigService,
          useValue: {
            get: (...args) => {},
          },
        },
        {
          provide: SettingsService,
          useValue: {},
        },
        {
          provide: AccountService,
          useValue: {},
        },
      ],
    }).compile();
    service = module.get<PeriodClosingService>(PeriodClosingService);
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
