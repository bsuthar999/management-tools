/* tslint:disable */

import { MigrationInterface, QueryRunner } from 'typeorm';

export class InitializeDB1545393094796 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TYPE "general_ledger_amounttype_enum" AS ENUM('CREDIT', 'DEBIT')`,
    );
    await queryRunner.query(
      `CREATE TABLE "general_ledger" ("transactionId" SERIAL NOT NULL, "transactionDate" TIMESTAMP NOT NULL, "amountType" "general_ledger_amounttype_enum" NOT NULL, "amount" double precision NOT NULL, "journalEntryTransactionId" integer, "accountId" integer, CONSTRAINT "PK_8fb6eb35c62f96b66b52d1b01b7" PRIMARY KEY ("transactionId"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "journal_entry" ("transactionId" SERIAL NOT NULL, "transactionDate" TIMESTAMP NOT NULL DEFAULT now(), "isLocked" boolean NOT NULL, CONSTRAINT "PK_f4f40315dce2169d0b492329bd8" PRIMARY KEY ("transactionId"))`,
    );
    await queryRunner.query(
      `CREATE TYPE "journal_entry_account_entrytype_enum" AS ENUM('CREDIT', 'DEBIT')`,
    );
    await queryRunner.query(
      `CREATE TABLE "journal_entry_account" ("id" SERIAL NOT NULL, "entryType" "journal_entry_account_entrytype_enum" NOT NULL, "amount" double precision NOT NULL, "createdBy" character varying NOT NULL, "journalEntryTransactionId" integer, "accountId" integer, CONSTRAINT "PK_69d670b4f272d335c76d6161043" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "account" ("id" SERIAL NOT NULL, "uuid" character varying NOT NULL, "accountNumber" character varying NOT NULL, "accountName" character varying NOT NULL, "accountType" character varying NOT NULL, "isGroup" boolean NOT NULL, "createdBy" character varying NOT NULL, "modifiedBy" character varying NOT NULL, "creation" TIMESTAMP NOT NULL DEFAULT now(), "modified" TIMESTAMP NOT NULL, "isRoot" boolean NOT NULL, "parentId" integer, CONSTRAINT "UQ_ee66d482ebdf84a768a7da36b08" UNIQUE ("accountNumber"), CONSTRAINT "PK_54115ee388cdb6d86bb4bf5b2ea" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_dbb3706133e6f1ac5dedc8e66e" ON "account" ("accountNumber", "uuid") `,
    );
    await queryRunner.query(
      `CREATE TABLE "account_statement" ("uuid" character varying NOT NULL, "closingBalance" double precision NOT NULL, "lastUpdated" TIMESTAMP NOT NULL DEFAULT now(), "accountId" integer NOT NULL, CONSTRAINT "REL_624e66365d69f7c0ba2d7bdb06" UNIQUE ("accountId"), CONSTRAINT "PK_714b1fb5c05820f02a80c21acfb" PRIMARY KEY ("uuid", "accountId"))`,
    );
    await queryRunner.query(
      `CREATE TYPE "general_ledger_archive_amounttype_enum" AS ENUM('CREDIT', 'DEBIT')`,
    );
    await queryRunner.query(
      `CREATE TABLE "general_ledger_archive" ("transactionId" SERIAL NOT NULL, "transactionDate" TIMESTAMP NOT NULL DEFAULT now(), "amountType" "general_ledger_archive_amounttype_enum" NOT NULL, "amount" double precision NOT NULL, "accountId" integer, CONSTRAINT "REL_f451cc46ecd07895426f57c487" UNIQUE ("accountId"), CONSTRAINT "PK_9d641620edefdb0ef7e1fb8572e" PRIMARY KEY ("transactionId"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "account_closure" ("id_ancestor" integer NOT NULL, "id_descendant" integer NOT NULL, CONSTRAINT "PK_314b38993b9c1030da4176dce5c" PRIMARY KEY ("id_ancestor", "id_descendant"))`,
    );
    await queryRunner.query(
      `ALTER TABLE "general_ledger" ADD CONSTRAINT "FK_d559230ad48ef2dc5371c3a2735" FOREIGN KEY ("journalEntryTransactionId") REFERENCES "journal_entry"("transactionId")`,
    );
    await queryRunner.query(
      `ALTER TABLE "general_ledger" ADD CONSTRAINT "FK_f39a93a7208f7f640fff8651aa2" FOREIGN KEY ("accountId") REFERENCES "account"("id")`,
    );
    await queryRunner.query(
      `ALTER TABLE "journal_entry_account" ADD CONSTRAINT "FK_3800883510b2fefb0f618a6c61f" FOREIGN KEY ("journalEntryTransactionId") REFERENCES "journal_entry"("transactionId")`,
    );
    await queryRunner.query(
      `ALTER TABLE "journal_entry_account" ADD CONSTRAINT "FK_94f3503d3932caa2899a5c89d2a" FOREIGN KEY ("accountId") REFERENCES "account"("id")`,
    );
    await queryRunner.query(
      `ALTER TABLE "account" ADD CONSTRAINT "FK_30d2fca3d68a5ab51c952dc9aab" FOREIGN KEY ("parentId") REFERENCES "account"("id")`,
    );
    await queryRunner.query(
      `ALTER TABLE "account_statement" ADD CONSTRAINT "FK_624e66365d69f7c0ba2d7bdb06a" FOREIGN KEY ("accountId") REFERENCES "account"("id")`,
    );
    await queryRunner.query(
      `ALTER TABLE "general_ledger_archive" ADD CONSTRAINT "FK_f451cc46ecd07895426f57c487b" FOREIGN KEY ("accountId") REFERENCES "account"("id")`,
    );
    await queryRunner.query(
      `ALTER TABLE "account_closure" ADD CONSTRAINT "FK_66463c417c83e3b1d683dd2d68d" FOREIGN KEY ("id_ancestor") REFERENCES "account"("id")`,
    );
    await queryRunner.query(
      `ALTER TABLE "account_closure" ADD CONSTRAINT "FK_c2c04cd35aca50f072d12ebe1ad" FOREIGN KEY ("id_descendant") REFERENCES "account"("id")`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "account_closure" DROP CONSTRAINT "FK_c2c04cd35aca50f072d12ebe1ad"`,
    );
    await queryRunner.query(
      `ALTER TABLE "account_closure" DROP CONSTRAINT "FK_66463c417c83e3b1d683dd2d68d"`,
    );
    await queryRunner.query(
      `ALTER TABLE "general_ledger_archive" DROP CONSTRAINT "FK_f451cc46ecd07895426f57c487b"`,
    );
    await queryRunner.query(
      `ALTER TABLE "account_statement" DROP CONSTRAINT "FK_624e66365d69f7c0ba2d7bdb06a"`,
    );
    await queryRunner.query(
      `ALTER TABLE "account" DROP CONSTRAINT "FK_30d2fca3d68a5ab51c952dc9aab"`,
    );
    await queryRunner.query(
      `ALTER TABLE "journal_entry_account" DROP CONSTRAINT "FK_94f3503d3932caa2899a5c89d2a"`,
    );
    await queryRunner.query(
      `ALTER TABLE "journal_entry_account" DROP CONSTRAINT "FK_3800883510b2fefb0f618a6c61f"`,
    );
    await queryRunner.query(
      `ALTER TABLE "general_ledger" DROP CONSTRAINT "FK_f39a93a7208f7f640fff8651aa2"`,
    );
    await queryRunner.query(
      `ALTER TABLE "general_ledger" DROP CONSTRAINT "FK_d559230ad48ef2dc5371c3a2735"`,
    );
    await queryRunner.query(`DROP TABLE "account_closure"`);
    await queryRunner.query(`DROP TABLE "general_ledger_archive"`);
    await queryRunner.query(
      `DROP TYPE "general_ledger_archive_amounttype_enum"`,
    );
    await queryRunner.query(`DROP TABLE "account_statement"`);
    await queryRunner.query(`DROP INDEX "IDX_dbb3706133e6f1ac5dedc8e66e"`);
    await queryRunner.query(`DROP TABLE "account"`);
    await queryRunner.query(`DROP TABLE "journal_entry_account"`);
    await queryRunner.query(`DROP TYPE "journal_entry_account_entrytype_enum"`);
    await queryRunner.query(`DROP TABLE "journal_entry"`);
    await queryRunner.query(`DROP TABLE "general_ledger"`);
    await queryRunner.query(`DROP TYPE "general_ledger_amounttype_enum"`);
  }
}
