export const SEND_EMAIL = 'send_email';
export const AUTHORIZATION = 'authorization';
export const ADMINISTRATOR = 'administrator';
export const TOKEN = 'token';
export const ACCOUNT_TYPES = [
  'ASSET',
  'LIABILITY',
  'INCOME',
  'EXPENSE',
  'EQUITY',
];
export const PERIOD_CLOSING = 'period_closing';
export const MONTHLY = 'monthly';
export const QUATERLY = 'quaterly';
export const HALF_YEARLY = 'half yearly';
export const YEARLY = 'yearly';
