import {
  Controller,
  Post,
  UseGuards,
  Param,
  Body,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { TokenGuard } from '../../gaurds/token.guard';
import { RoleGuard } from '../../gaurds/role.guard';
import { Roles } from '../../decorators/roles.decorator';
import { ADMINISTRATOR } from '../../constants/roles';
import { SetupService } from './setup.service';
import { MANAGEMENT_CONSOLE } from '../../models/settings/service-type';
import { SettingsDto } from '../../models/settings/settings.dto';

@Controller('setup')
export class SetupController {
  constructor(private readonly setupService: SetupService) {}

  @Post()
  @UsePipes(ValidationPipe)
  async setup(@Body() payload: SettingsDto) {
    const settings = Object.assign({}, payload);
    settings.type = MANAGEMENT_CONSOLE;
    await this.setupService.setup(payload);
  }

  @Post('service/:type')
  @Roles(ADMINISTRATOR)
  @UseGuards(TokenGuard, RoleGuard)
  async setupServiceByType(@Param('type') type, @Body('appURL') appURL) {
    return await this.setupService.setupServiceByType(type, appURL);
  }
}
