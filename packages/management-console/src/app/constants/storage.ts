export const CLIENT_ID = 'client_id';
export const REDIRECT_URI = 'redirect_uri';
export const OPENID_ROLES = 'openid roles';
export const SILENT_REFRESH_REDIRECT_URI = 'silent_refresh_redirect_uri';
export const LOGIN_URL = 'login_url';
export const ISSUER_URL = 'issuer_url';
export const APP_URL = 'app_url';
export const USER_UUID = 'user_uuid';
export const NEW_ID: string = 'new';
export const JE_CREATED: string = 'Journal Entry successfully created';
export const JE_ERROR: string = 'Journal Entry creation failed';
export const ACCOUNT_TYPES = [
  'ASSET',
  'LIABILITY',
  'INCOME',
  'EXPENSE',
  'EQUITY',
];
export const ENTRY_TYPE = ['CREDIT', 'DEBIT'];
export const SERVICES = 'services';
export const ACCOUNTING_SERVER = 'accounting-server';
export const EXISTS = 'Exists';
export const ACCOUNT_SETUP = 'account_setup';
export const PERIOD_CLOSING = ' Period Closing ';
export const UPDATED = ' updated ';
export const CREATED = ' created ';
export const CLOSE = 'Close';
export const ERROR = 'Error';
export const JOURNAL_ENTRY = 'Journal Entry';
export const CREDIT = 'Credit';
export const DEBIT = 'Debit';
export const DISPLAYED_COLUMNS = ['account', 'entryType', 'amount', 'button'];
export const ELEMENT_DATA = [
  {
    account: '',
    entryType: 'CREDIT',
    amount: 0,
  },
];
export const LISTING_COLUMN = [
  'accountName',
  'accountNumber',
  'accountType',
  'isGroup',
];
export const CLOSING_OPTIONS = ['monthly', 'quaterly', 'half yearly', 'yearly'];
