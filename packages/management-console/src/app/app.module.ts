import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { LayoutModule } from '@angular/cdk/layout';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MaterialModule } from './material/material.module';
import { NavigationComponent } from './navigation/navigation.component';
import { OAuthModule, OAuthStorage } from 'angular-oauth2-oidc';
import { AuthGuard } from './guards/auth.guard';
import { HomeComponent } from './home/home.component';
import { AppService } from './app.service';
import { ListingComponent } from './listing/listing.component';
import { ListingService } from './common/listing.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpErrorHandler } from './http-error-handler.service';
import { MessageService } from './message.service';
import { AccountComponent } from './account/account.component';
import { JournalEntryComponent } from './journal-entry/journal-entry.component';
import { AuthService } from './common/auth.service';
// import { TreeService } from './tree/tree.service';
import { SettingsComponent } from './settings/settings.component';
// import { TreeNode } from './tree/tree.node';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    HomeComponent,
    ListingComponent,
    AccountComponent,
    JournalEntryComponent,
    // TreeNode,
    SettingsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    LayoutModule,
    HttpClientModule,
    MaterialModule,
    ReactiveFormsModule,
    ScrollingModule,
    FormsModule,
    OAuthModule.forRoot(),
    FlexLayoutModule,
    NgxDatatableModule,
  ],
  providers: [
    AuthGuard,
    AppService,
    HttpErrorHandler,
    MessageService,
    ListingService,
    AuthService,
    { provide: OAuthStorage, useValue: localStorage },
    // TreeService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
