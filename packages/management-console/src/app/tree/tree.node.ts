// import { Component } from '@angular/core';
// import { TreeDatabase } from './tree.datasource';
// import { FlatTreeControl } from '@angular/cdk/tree';
// import { TreeDataSource, TreeFlatNode } from './tree.component';

// @Component({
//   selector: 'app-tree',
//   templateUrl: './tree.component.html',
//   styleUrls: ['./tree.component.css'],
//   providers: [TreeDatabase],
// })
// export class TreeNode {
//   constructor(database: TreeDatabase) {
//     this.treeControl = new FlatTreeControl<TreeFlatNode>(
//       this.getLevel,
//       this.isExpandable,
//     );
//     this.dataSource = new TreeDataSource(this.treeControl, database);

//     this.dataSource.data = database.initialData();
//   }

//   treeControl: FlatTreeControl<TreeFlatNode>;

//   dataSource: TreeDataSource;

//   getLevel = (node: TreeFlatNode) => node.level;

//   isExpandable = (node: TreeFlatNode) => node.expandable;

//   hasChild = (_: number, _nodeData: TreeFlatNode) => _nodeData.expandable;
// }
