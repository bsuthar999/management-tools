// import { Injectable } from '@angular/core';
// import { AuthService } from '../common/auth.service';
// import { HttpClient } from '@angular/common/http';
// import { StorageService } from '../common/storage.service';
// import { SERVICES, ACCOUNTING } from '../constants/storage';
// import { map } from 'rxjs/operators';

// @Injectable({
//   providedIn: 'root',
// })
// export class TreeService {
//   accountingServerUrl = this.storageService.getServiceURL(ACCOUNTING);
//   constructor(
//     private authService: AuthService,
//     private http: HttpClient,
//     private storageService: StorageService,
//   ) {}

//   getChildrens(accountNumber) {
//     const model = 'account';
//     return this.http
//       .get<any>(
//         `${this.accountingServerUrl}/${model}/v1/tree/${accountNumber}`,
//         {
//           headers: this.authService.headers,
//         },
//       )
//       .pipe(map(response => response.map(ac => ac.accountNumber)));
//   }
// }
