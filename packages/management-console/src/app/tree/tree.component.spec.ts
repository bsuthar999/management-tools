// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { MaterialModule } from '../material/material.module';
// import { TreeDatabase } from './tree.datasource';
// import { OAuthService } from 'angular-oauth2-oidc';
// import { oauthServiceStub } from '../common/testing-helpers.1';
// import { HttpClientTestingModule } from '@angular/common/http/testing';
// import { RouterTestingModule } from '@angular/router/testing';
// import { TreeNode } from './tree.node';
// import { TreeFlatNode } from './tree.component';
// import { StorageService } from '../common/storage.service';

// describe('TreeComponent', () => {
//   let component: TreeNode;
//   let fixture: ComponentFixture<TreeNode>;

//   beforeEach(async(() => {
//     TestBed.configureTestingModule({
//       imports: [MaterialModule, HttpClientTestingModule, RouterTestingModule],
//       declarations: [TreeNode],
//       providers: [
//         {
//           provide: TreeDatabase,
//           useValue: {},
//         },
//         {
//           provide: TreeFlatNode,
//           useValue: {},
//         },
//         {
//           provide: OAuthService,
//           useValue: oauthServiceStub,
//         },
//         {
//           provide: StorageService,
//           useValue: {
//             getServiceURL: (...args) => 'localhost',
//             getInfo: (...args) => ({}),
//           },
//         },
//       ],
//     }).compileComponents();
//   }));

//   beforeEach(() => {
//     fixture = TestBed.createComponent(TreeNode);
//     component = fixture.componentInstance;
//     fixture.detectChanges();
//   });

//   it('should create', () => {
//     expect(component).toBeTruthy();
//   });
// });
