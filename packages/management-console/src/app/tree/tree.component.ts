// import { CollectionViewer, SelectionChange } from '@angular/cdk/collections';
// import { FlatTreeControl } from '@angular/cdk/tree';
// import { Injectable } from '@angular/core';
// import { BehaviorSubject, merge, Observable } from 'rxjs';
// import { map } from 'rxjs/operators';
// import { TreeDatabase } from './tree.datasource';

// export class TreeFlatNode {
//   constructor(
//     public item: string,
//     public level = 1,
//     public expandable = false,
//     public isLoading = false,
//   ) {}
// }

// @Injectable()
// export class TreeDataSource {
//   dataChange = new BehaviorSubject<TreeFlatNode[]>([]);

//   get data(): TreeFlatNode[] {
//     return this.dataChange.value;
//   }
//   set data(value: TreeFlatNode[]) {
//     this.treeControl.dataNodes = value;
//     this.dataChange.next(value);
//   }

//   constructor(
//     private treeControl: FlatTreeControl<TreeFlatNode>,
//     private database: TreeDatabase,
//   ) {}

//   connect(collectionViewer: CollectionViewer): Observable<TreeFlatNode[]> {
//     this.treeControl.expansionModel.onChange.subscribe(change => {
//       if (
//         (change as SelectionChange<TreeFlatNode>).added ||
//         (change as SelectionChange<TreeFlatNode>).removed
//       ) {
//         this.handleTreeControl(change as SelectionChange<TreeFlatNode>);
//       }
//     });

//     return merge(collectionViewer.viewChange, this.dataChange).pipe(
//       map(() => this.data),
//     );
//   }

//   handleTreeControl(change: SelectionChange<TreeFlatNode>) {
//     if (change.added) {
//       change.added.forEach(node => this.toggleNode(node, true));
//     }
//     if (change.removed) {
//       change.removed
//         .slice()
//         .reverse()
//         .forEach(node => this.toggleNode(node, false));
//     }
//   }

//   toggleNode(node: TreeFlatNode, expand: boolean) {
//     const children = this.database.getChildren(node.item);
//     const index = this.data.indexOf(node);
//     if (!children || index < 0) {
//       // If no children, or cannot find the node, no op
//       return;
//     }

//     node.isLoading = true;

//     setTimeout(() => {
//       if (expand) {
//         const nodes = children.map(
//           name =>
//             new TreeFlatNode(
//               name,
//               node.level + 1,
//               this.database.isExpandable(name),
//             ),
//         );
//         this.data.splice(index + 1, 0, ...nodes);
//       } else {
//         let count = 0;
//         for (
//           let i = index + 1;
//           i < this.data.length && this.data[i].level > node.level;
//           i++, count++
//         ) {}
//         this.data.splice(index + 1, count);
//       }

//       // notify the change
//       this.dataChange.next(this.data);
//       node.isLoading = false;
//     });
//   }
// }
