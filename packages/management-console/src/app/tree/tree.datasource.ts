// import { TreeFlatNode } from './tree.component';
// import { TreeService } from './tree.service';
// import { Injectable } from '@angular/core';

// @Injectable()
// export class TreeDatabase {
//   constructor(private readonly treeService: TreeService) {}

//   dataMap = new Map<string, string[]>([
//     ['100000000', []],
//     ['200000000', []],
//     ['300000000', []],
//     ['500000000', []],
//     ['500000000', []],
//   ]);

//   rootLevelNodes: string[] = [
//     '100000000',
//     '200000000',
//     '300000000',
//     '400000000',
//     '500000000',
//   ];

//   /** Initial data from database */
//   initialData(): TreeFlatNode[] {
//     return this.rootLevelNodes.map(name => new TreeFlatNode(name, 0, true));
//   }

//   getChildren(node: string): string[] | undefined {
//     this.treeService.getChildrens(node).subscribe({
//       next: res => {
//         this.dataMap.set(node, res);
//       },
//     });
//     return this.dataMap.get(node);
//   }

//   isExpandable(node: string): true {
//     return true;
//   }
// }
