import { TestBed } from '@angular/core/testing';

import { SettingsService } from './settings.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AuthService } from '../common/auth.service';
import { StorageService } from '../common/storage.service';

describe('SettingsService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: AuthService,
          useValue: {},
        },

        {
          provide: StorageService,
          useValue: {
            getInfo: (...args) => '',
            getServiceURL: (...args) => '',
          },
        },
      ],
    }),
  );

  it('should be created', () => {
    const service: SettingsService = TestBed.get(SettingsService);
    expect(service).toBeTruthy();
  });
});
