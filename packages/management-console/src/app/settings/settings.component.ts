import { Component, OnInit } from '@angular/core';
import { SettingsService } from './settings.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import {
  CLOSING_OPTIONS,
  PERIOD_CLOSING,
  UPDATED,
  CLOSE,
} from './../constants/storage';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css'],
})
export class SettingsComponent implements OnInit {
  periodClosing: string;
  closingOptions: string[];
  settingsForm: FormGroup;
  constructor(
    private readonly snackbar: MatSnackBar,
    private readonly settingsService: SettingsService,
    private readonly formBuilder: FormBuilder,
  ) {}

  ngOnInit() {
    this.settingsForm = this.formBuilder.group({
      periodClosing: '',
    });
    this.closingOptions = CLOSING_OPTIONS;
    this.settingsService.getSettings().subscribe({
      next: (response: any[]) => {
        this.populateSettingsForm(response);
      },
    });
  }

  setPeriodClosing() {
    const newClosing = this.settingsForm.controls.periodClosing.value;
    this.settingsService.setPeriodClosing(newClosing).subscribe({
      next: (response: any[]) => {
        this.snackbar.open(PERIOD_CLOSING + UPDATED, CLOSE, {
          duration: 2500,
        });
      },
    });
  }

  populateSettingsForm(response) {
    this.settingsForm.controls.periodClosing.setValue(
      response[0].periodClosing,
    );
  }
}
