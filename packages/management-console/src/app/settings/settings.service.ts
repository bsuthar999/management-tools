import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { StorageService } from '../common/storage.service';
import { APP_URL, ACCOUNTING_SERVER } from '../constants/storage';
import { AuthService } from '../common/auth.service';

@Injectable({
  providedIn: 'root',
})
export class SettingsService {
  settings = this.storageService.getServiceURL(ACCOUNTING_SERVER);
  model = 'settings';
  issuer = this.storageService.getInfo(APP_URL);
  common = `${this.issuer}/${this.model}`;
  constructor(
    private readonly http: HttpClient,
    private readonly oauthService: AuthService,
    private readonly storageService: StorageService,
  ) {}

  setupAccount() {
    const url = `${this.issuer}/account-setup/v1/oneTimeSetup`;
    return this.http.post(url, undefined, {
      headers: this.oauthService.headers,
    });
  }

  getSettings() {
    const url = `${this.settings}/settings/v1/getSettings`;
    const accountSettings = this.http.get(url, {
      headers: this.oauthService.headers,
    });
    return accountSettings;
  }

  setPeriodClosing(param) {
    const url = `${this.settings}/settings/v1/periodClosing`;
    return this.http.post(
      url,
      { body: { periodClosing: param } },
      { headers: this.oauthService.headers },
    );
  }
}
