import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { filter, map } from 'rxjs/operators';
import { CreateAccountRespose } from '../interface/account-response.interface';
import {
  ACCOUNT_CREATED,
  ACCOUNT_ERROR,
  ACCOUNT_UPDATED,
} from '../constants/messages';
import { NEW_ID, ACCOUNT_TYPES, CLOSE } from '../constants/storage';
import { FormService } from '../common/form.service';
import { ListingService } from '../common/listing.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css'],
})
export class AccountComponent implements OnInit {
  uuid: string;
  model: string;
  search: string = '';
  accountNumber: string;
  accountName: string;
  accountType: string;
  isGroup: boolean;
  parent: string;
  types = ACCOUNT_TYPES;
  accounts;

  accountForm: FormGroup;

  constructor(
    private formService: FormService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private snackbar: MatSnackBar,
    private listService: ListingService,
  ) {
    this.uuid = this.activatedRoute.snapshot.params.uuid;
    this.router.events
      .pipe(filter(route => route instanceof NavigationEnd))
      .subscribe((route: NavigationEnd) => {
        this.model = route.url.split('/')[1];
      });
  }

  ngOnInit() {
    this.accountForm = this.formBuilder.group({
      accountNumber: '',
      accountName: '',
      accountType: '',
      isGroup: '',
      parent: '',
    });

    this.subscribeListAccounts();

    if (this.uuid && this.uuid !== NEW_ID) {
      this.subscribeGetAccount(this.uuid);
    } else {
      this.accountForm.controls.isGroup.enable();
    }
  }

  searchKeyUp() {
    this.search = this.accountForm.controls.parent.value;
    this.subscribeListAccounts();
  }

  subscribeListAccounts() {
    this.accounts = this.listService
      .findModels(this.model, this.search, '', 0, 10, `"isGroup" = true`)
      .pipe(
        map((resp: any) => {
          return resp.docs.map(account => account.accountNumber);
        }),
      );
  }

  subscribeGetAccount(uuid: string) {
    this.formService.getAccount(this.model, this.uuid).subscribe({
      next: response => {
        if (response) {
          this.populateAccountForm(response);
        }
      },
    });
  }

  createAccount() {
    this.formService
      .createAccount(
        this.model,
        this.accountForm.controls.accountNumber.value,
        this.accountForm.controls.accountName.value,
        this.accountForm.controls.accountType.value,
        this.accountForm.controls.isGroup.value || false,
        this.accountForm.controls.parent.value,
      )
      .subscribe({
        next: (accountResponse: CreateAccountRespose) => {
          // this.router.navigate(['/account'])
          this.accountForm.controls.parent.setValue('');
          this.snackbar.open(ACCOUNT_CREATED, CLOSE, { duration: 2500 });

          this.router.navigateByUrl('/account');
        },
        error: error => {
          this.snackbar.open(ACCOUNT_ERROR, CLOSE, { duration: 2500 });
        },
      });
  }

  updateAccount() {
    this.formService
      .updateAccount(
        this.uuid,
        this.model,
        this.accountForm.controls.accountName.value,
        this.accountForm.controls.accountNumber.value,
      )
      .subscribe({
        next: response => {
          this.accountName = response[0].accountName;
          this.snackbar.open(ACCOUNT_UPDATED, CLOSE, { duration: 2500 });
        },
      });
  }

  populateAccountForm(account) {
    this.accountNumber = account.accountNumber;
    this.accountName = account.accountName;
    this.accountType = account.accountType;
    this.isGroup = account.isGroup;
    this.parent = account.parent;
    this.accountForm.controls.accountNumber.setValue(account.accountNumber);
    this.accountForm.controls.accountName.setValue(account.accountName);
    this.accountForm.controls.accountType.setValue(account.accountType);
    this.accountForm.controls.parent.setValue(account.parent);
    this.accountForm.controls.isGroup.setValue(account.isGroup);
    this.accountForm.controls.accountType.disable();
    this.accountForm.controls.isGroup.disable();
    this.accountForm.controls.accountNumber.disable();
    this.accountForm.controls.parent.disable();
  }
}
