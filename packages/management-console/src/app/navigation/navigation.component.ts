import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {
  ISSUER_URL,
  APP_URL,
  ACCOUNT_SETUP,
  CREATED,
  CLOSE,
  EXISTS,
  ERROR,
} from '../constants/storage';
import { OAuthService, OAuthEvent } from 'angular-oauth2-oidc';
import { Router } from '@angular/router';
import { StorageService } from '../common/storage.service';
import { ADMINISTRATOR } from '../constants/roles';
import { IDTokenClaims } from '../interface/id-token-claims.interfaces';
import { SettingsService } from '../settings/settings.service';
import { MatSnackBar } from '@angular/material';
import { AppService } from '../app.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css'],
})
export class NavigationComponent {
  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(map(result => result.matches));
  accountSetupComplete: boolean;
  tokenIsValid: boolean;
  loggedIn: boolean;

  constructor(
    private appService: AppService,
    private snackbar: MatSnackBar,
    private settingsService: SettingsService,
    private breakpointObserver: BreakpointObserver,
    private oauthService: OAuthService,
    private router: Router,
    private storageService: StorageService,
  ) {}

  ngOnInit(): void {
    this.accountSetupComplete = JSON.parse(
      this.storageService.getInfo(ACCOUNT_SETUP) || 'false',
    );
    this.oauthService.events.subscribe(({ type }: OAuthEvent) => {
      // Silent Refresh
      switch (type) {
        case 'token_received':
          this.setUserSession();
          this.router.navigate(['dashboard']);
          break;
      }
    });
    this.setUserSession();
  }

  accountSetup() {
    if (this.storageService.getInfo(ACCOUNT_SETUP) === 'true') {
      this.snackbar.open(ACCOUNT_SETUP + EXISTS, CLOSE, {
        duration: 2500,
      });
      this.accountSetupComplete = true;
    } else {
      this.settingsService.setupAccount().subscribe({
        next: res => {
          this.snackbar.open(ACCOUNT_SETUP + CREATED, CLOSE, {
            duration: 2500,
          });
        },
        error: err => {
          this.snackbar.open(ERROR, CLOSE, {
            duration: 2500,
          });
        },
      });
      this.snackbar.open(ACCOUNT_SETUP + CREATED, CLOSE, {
        duration: 2500,
      });
      this.appService.getMessage();
    }
  }

  logout() {
    const logOutUrl =
      this.storageService.getInfo(ISSUER_URL) +
      '/auth/logout?redirect=' +
      this.storageService.getInfo(APP_URL);
    this.storageService.clearInfoLocalStorage();
    this.oauthService.logOut();
    this.tokenIsValid = false;
    window.location.href = logOutUrl;
  }

  setUserSession() {
    const idClaims: IDTokenClaims = this.oauthService.getIdentityClaims() || {
      roles: [],
    };
    this.tokenIsValid = idClaims.roles.includes(ADMINISTRATOR);
    this.loggedIn = this.oauthService.hasValidAccessToken();
  }
}
