import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { switchMap, map } from 'rxjs/operators';
import { StorageService } from './common/storage.service';
import { ACCOUNTING_SERVER, ACCOUNT_SETUP } from './constants/storage';

@Injectable()
export class AppService {
  messageUrl = '/info'; // URL to web api

  constructor(private http: HttpClient, private storage: StorageService) {}

  /** GET message from the server */
  getMessage(): Observable<any> {
    return this.http.get<any>(this.messageUrl).pipe(
      switchMap(appInfo => {
        return this.http.get<any>(appInfo.authServerURL + '/info').pipe(
          map(authInfo => {
            appInfo.services = authInfo.services;
            return appInfo;
          }),
        );
      }),
    );
  }

  getAccountingInfo() {
    const serviceURL = this.storage.getServiceURL(ACCOUNTING_SERVER);
    this.http.get<any>(serviceURL + '/info').subscribe({
      next: success => {
        localStorage.setItem(
          ACCOUNT_SETUP,
          JSON.stringify(success.accountSetup),
        );
      },
      error: fail => {
        localStorage.setItem(ACCOUNT_SETUP, JSON.stringify(false));
      },
    });
  }
}
