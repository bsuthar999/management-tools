import { Injectable } from '@angular/core';
import { StorageService } from './storage.service';
import { ACCOUNTING_SERVER } from '../constants/storage';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HandleError, HttpErrorHandler } from '../http-error-handler.service';
import { AuthService } from './auth.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ListingService {
  handleError: HandleError;

  constructor(
    private storageService: StorageService,
    httpErrorHandler: HttpErrorHandler,
    private http: HttpClient,
    private authService: AuthService,
  ) {
    this.handleError = httpErrorHandler.createHandleError('ListingService');
  }

  findModels(
    model: string,
    filters = '',
    sortOrder = 'ASC',
    pageNumber = 0,
    pageSize = 10,
    query = 'id > 0',
  ) {
    const appUrl = this.storageService.getServiceURL(ACCOUNTING_SERVER);
    let url: string;
    if (model === 'journalentry') {
      url = `${appUrl}/${model}/v1/list/all`;
    } else {
      url = `${appUrl}/${model}/v1/list`;
    }

    const params = new HttpParams()
      .set('limit', pageSize.toString())
      .set('offset', (pageNumber * pageSize).toString())
      .set('search', filters)
      .set('sort', sortOrder)
      .set('filters', query);

    return this.http
      .get(url, { params, headers: this.authService.headers })
      .pipe(
        map(data => {
          return data;
        }),
      );
  }
}
