import { Injectable } from '@angular/core';
import { ACCOUNTING_SERVER } from '../constants/storage';
import { StorageService } from '../common/storage.service';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../common/auth.service';

@Injectable({
  providedIn: 'root',
})
export class JournalEntryService {
  model = 'journalentry';
  constructor(
    private readonly storageService: StorageService,
    private readonly http: HttpClient,
    private readonly oauthService: AuthService,
  ) {}

  getJe(model, journalEntryTransactionId) {
    const accountServer = this.storageService.getServiceURL(ACCOUNTING_SERVER);
    const url = `${accountServer}/${
      this.model
    }/v1/getOne/${journalEntryTransactionId}`;
    // TODO: Interface for je response;
    return this.http.get<any>(url, { headers: this.oauthService.headers });
  }

  createJe(rows) {
    const accountServer = this.storageService.getServiceURL(ACCOUNTING_SERVER);
    const url = `${accountServer}/${this.model}/v1/create`;
    return this.http.post(
      url,
      { accounts: rows },
      { headers: this.oauthService.headers },
    );
  }
}
