import { TestBed } from '@angular/core/testing';

import { JournalEntryService } from './journal-entry.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpErrorHandler } from '../http-error-handler.service';
import { AuthService } from '../common/auth.service';

describe('JournalEntryService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: AuthService,
          useValue: {},
        },
        {
          provide: HttpErrorHandler,
          useValue: {
            handleError<T>(...args) {},
            createHandleError(...args) {},
          },
        },
        {
          provide: JournalEntryService,
          useValue: JournalEntryService,
        },
      ],
    }),
  );
  it('should be created', () => {
    const service: JournalEntryService = TestBed.get(JournalEntryService);
    expect(service).toBeTruthy();
  });
});
