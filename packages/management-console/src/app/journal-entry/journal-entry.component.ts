import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { filter, map } from 'rxjs/operators';
import {
  ENTRY_TYPE,
  CREATED,
  CLOSE,
  JOURNAL_ENTRY,
  DEBIT,
  CREDIT,
  DISPLAYED_COLUMNS,
  ELEMENT_DATA,
} from '../constants/storage';
import { JournalEntryService } from './journal-entry.service';
import { ListingService } from '../common/listing.service';
import { CreateJeResponse } from './journal-entry.interface';
import {
  MatSnackBar,
  MatTableDataSource,
  MatPaginator,
  MatSort,
} from '@angular/material';

@Component({
  selector: 'app-journalentry',
  templateUrl: './journal-entry.component.html',
  styleUrls: ['./journal-entry.component.css'],
})
export class JournalEntryComponent implements OnInit {
  uuid: string;
  model: string;
  entryType: string;
  debitSum: number = 0;
  creditSum: number = 0;
  amount: number;
  account: string;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  journalEntryTransactionId: number;
  types = ENTRY_TYPE;
  filter: string;
  sortOrder: 'ASC' | 'DSC';
  pageNumber: number;
  pageSize: number;
  query: string;
  displayedColumns = DISPLAYED_COLUMNS;
  elementData = ELEMENT_DATA;
  dataSource = new MatTableDataSource(this.elementData);

  jeForm: FormGroup;
  editing = {};
  responseData: any[] = [];
  data;
  dropDownData: any[] | any;
  createJeDto: CreateJeResponse;

  constructor(
    private journalEntryService: JournalEntryService,
    private router: Router,
    private snackbar: MatSnackBar,
    private accountService: ListingService,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder, // private snackbar: MatSnackBar,
  ) {
    // TODO: get data from server

    this.router.events
      .pipe(filter(route => route instanceof NavigationEnd))
      .subscribe((route: NavigationEnd) => {
        this.model = route.url.split('/')[1];
      });
  }

  ngOnInit() {
    if (this.activatedRoute.snapshot.params.id !== 'new') {
      this.journalEntryTransactionId = this.activatedRoute.snapshot.params.id;
    }

    this.dropDownData = this.accountService
      .findModels(
        (this.model = 'account'),
        this.filter,
        this.sortOrder,
        this.pageNumber,
        this.pageSize,
        (this.query = `"isGroup" = 'false'`),
      )
      .pipe(
        map((data: any) => {
          this.total(this.dataSource.data);
          return data.docs;
        }),
      );

    if (this.journalEntryTransactionId) {
      this.dataSource.data = this.responseData;
      this.total(this.dataSource.data);
    }

    this.jeForm = this.formBuilder.group({
      entryType: '',
      amount: 0,
      account: 1,
    });

    if (this.journalEntryTransactionId) {
      this.total(this.dataSource.data);
      this.journalEntryService
        .getJe(this.model, this.journalEntryTransactionId)
        .subscribe({
          next: response => {
            this.dataSource.data = response;
            this.dataSource._updateChangeSubscription();
            this.total(this.dataSource.data);
          },
        });
    }
  }
  removeRow(index) {
    this.dataSource.data.splice(index, 1);
    this.dataSource._updateChangeSubscription();
    this.total(this.dataSource.data);
  }

  addRow() {
    this.elementData.push({
      account: '',
      entryType: '',
      amount: 0,
    });
    this.total(this.dataSource.data);
    this.dataSource._updateChangeSubscription();
  }

  applyFilter(filterValue: string) {
    this.total(this.dataSource.data);
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  total(data) {
    this.creditSum = 0;
    this.debitSum = 0;
    data.forEach(element => {
      if (element.entryType === CREDIT) this.creditSum += element.amount;
      if (element.entryType === DEBIT) this.debitSum += element.amount;
    });
  }

  createJe() {
    this.journalEntryService.createJe(this.dataSource.data).subscribe({
      next: res => {
        if (res) {
          this.snackbar.open(JOURNAL_ENTRY + CREATED, CLOSE, {
            duration: 2500,
          });
          this.router.navigateByUrl('/journalentry');
        }
      },
      error: err => {
        this.snackbar.open('err', CLOSE, { duration: 2500 });
      },
    });
  }
}
