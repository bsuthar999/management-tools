export interface CreateJeResponse {
  entryType: string | 'DEBIT';
  amount: number;
  account: string;
}
