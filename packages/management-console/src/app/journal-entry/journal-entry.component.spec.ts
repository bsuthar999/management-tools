import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JournalEntryComponent } from './journal-entry.component';
import { RouterTestingModule } from '@angular/router/testing';
import { MaterialModule } from '../material/material.module';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { JournalEntryService } from './journal-entry.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { AuthService } from '../common/auth.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpErrorHandler } from '../http-error-handler.service';
import { StorageService } from '../common/storage.service';
// import { of } from 'rxjs';

describe('JournalEntryComponent', () => {
  let component: JournalEntryComponent;
  let fixture: ComponentFixture<JournalEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [JournalEntryComponent],
      imports: [
        NoopAnimationsModule,
        MaterialModule,
        HttpClientTestingModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        NgxDatatableModule,
      ],
      providers: [
        {
          provide: AuthService,
          useValue: {},
        },
        {
          provide: JournalEntryService,
          useValue: {
            getJe: (...args) => '',
            getNewJE: (...args) => '',
          },
        },
        {
          provide: HttpErrorHandler,
          useValue: {
            createHandleError: (...args) => {},
          },
        },
        {
          provide: StorageService,
          useValue: {
            getServiceURL: (...args) => ({}),
          },
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JournalEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
