import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ListingComponent } from './listing/listing.component';
import { AuthGuard } from './guards/auth.guard';
import { AccountComponent } from './account/account.component';
import { JournalEntryComponent } from './journal-entry/journal-entry.component';
import { SettingsComponent } from './settings/settings.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },

  { path: 'account', component: ListingComponent },

  {
    path: 'account/:uuid',
    component: AccountComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'settings',
    component: SettingsComponent,
    canActivate: [AuthGuard],
  },
  // {
  //   path: 'tree/account',
  //   component: TreeNode,
  //   canActivate: [AuthGuard],
  // },
  {
    path: 'journalentry',
    component: ListingComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'journalentry/:id',
    component: JournalEntryComponent,
    canActivate: [AuthGuard],
  },

  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: '**', redirectTo: 'home' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
