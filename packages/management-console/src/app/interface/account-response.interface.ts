export interface CreateAccountRespose {
  accountNumber: string;
  accountName: string;
  accountType: string;
  parent: string;
  isGroup: boolean;
  uuid: string;
}
