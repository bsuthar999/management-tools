#!/usr/bin/env bash
echo "Building Accounting Server Docs"
./node_modules/.bin/typedoc\
    --out public/api/accounting-server \
    packages/accounting-server/src \
    --name "Accounting Server"

echo "Building Accounting Client Docs"
./node_modules/.bin/typedoc \
    --out public/api/management-console-client \
    packages/management-console/src \
    --name "Management Console Client"

echo "Building Accounting Client Docs"
./node_modules/.bin/typedoc \
    --out public/api/management-console-server \
    packages/management-console/server \
    --name "Management Console Server"
