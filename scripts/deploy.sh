#!/bin/bash

# Check helm chart is installed or create
# reuse installed values and resets data

export CHECK_AS=$(helm ls -q accounting-server-staging --tiller-namespace management-tools)
if [ "$CHECK_AS" = "accounting-server-staging" ]
then
    echo "Updating existing accounting-server-staging . . ."
    helm upgrade accounting-server-staging \
        --tiller-namespace management-tools \
        --namespace management-tools \
        --reuse-values \
        --recreate-pods \
        --wait \
        helm-charts/accounting-server
fi

export CHECK_MC=$(helm ls -q management-console-staging --tiller-namespace management-tools)
if [ "$CHECK_MC" = "management-console-staging" ]
then
    echo "Updating existing management-console-staging . . ."
    helm upgrade management-console-staging \
        --tiller-namespace management-tools \
        --namespace management-tools \
        --reuse-values \
        --recreate-pods \
        --wait \
        helm-charts/management-console
fi
