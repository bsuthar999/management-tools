# Development

### Setup Backing Services

#### Standard setup

Install postgres locally using your OS specific installation for Postgres.

#### Docker setup

Run following command as docker allowed user or root to start mongodb container.

```sh
docker run --name acc-postgres \
    -e POSTGRESQL_PASSWORD=secret \
    -e POSTGRESQL_DATABASE=accounting \
    -e POSTGRESQL_USERNAME=postgres \
    -p "5432:5432" \
    -d bitnami/postgresql:latest
```

In both setup cases start backing services before app development starts.

### Setup Environment

`.env` file for initializing following variables needs to be setup in project root to configure environment. This file is ignored by git.

```
POSTGRESDB_HOST=localhost
POSTGRESDB_NAME=accounting
POSTGRESDB_USER=localhost
POSTGRESDB_PASSWORD=secret
MONGODB_HOST=localhost
MONGODB_NAME=accounting
BULL_QUEUE_REDIS_HOST=localhost
BULL_QUEUE_REDIS_PORT=6379
```

Note: It is important to change the secrets and password. DO NOT USE example passwords or secrets.

### Setup hosts file

add `127.0.0.1 accounting.localhost` in `/etc/hosts` file or hosts file of your operating system.

### Setup server with POST request

Use `clientId` and `clientSecret` from response of `authorization-server` setup.

```
curl -d "appURL=http://accounting.localhost:4100" \
    -d "authServerURL=http://accounts.localhost:3000" \
    -d "clientId=d318d6cb-2b60-4afa-bd1c-9b9f9fa068a2" \
    -d "clientSecret=472188a19a11e6702c9aec54d86e42113b305f966d683147329fbba111454826" \
    -X POST http://accounting.localhost:4100/setup \
    -H "Content-Type: application/x-www-form-urlencoded"
```

### Start Server

```
> accounting/packages/server$ yarn start:server
```
